FROM alpine as tinytex

# tinytex dependencies
RUN apk --no-cache add \
  perl  \
  wget \
  xz \
  tar \
  fontconfig \
  freetype \
  lua \
  gcc

# setup workdir
WORKDIR /root

# download and install tinytex
RUN wget -qO- "https://yihui.name/gh/tinytex/tools/install-unx.sh" | sh

ENV PATH=/root/.TinyTeX/bin/x86_64-linuxmusl/:$PATH

RUN tlmgr install scheme-basic latexmk texcount




FROM node:10-alpine as app

WORKDIR /app
RUN apk add --no-cache git python3 make g++
RUN git clone https://gitlab.com/bauhaus/tex/mirror/clsi.git /app/

RUN npm ci --quiet



FROM node:10-alpine

RUN apk --no-cache add \
  tini \
  perl  \
  fontconfig \
  freetype \
  lua

COPY --from=tinytex /root/.TinyTeX /usr/local/tinytex
ENV PATH=/usr/local/tinytex/bin/x86_64-linuxmusl/:$PATH

COPY --from=app /app /app
COPY ./settings.js /app/config/settings.js

ENV SHARELATEX_CONFIG=/app/config/settings.js

ENTRYPOINT ["/sbin/tini", "--"]

WORKDIR /app

CMD ["node", "--expose-gc", "app.js"]