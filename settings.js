module.exports = {
  internal: {
    clsi: {
      host: process.env.LISTEN_ADDRESS || "0.0.0.0",
    },
  },
};
